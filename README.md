# Zynq UltraScale+ MPSoC Control via AXI interface in Linux User Space

Simple project to control **`Kria Zynq UltraScale+ MPSoC on KV260`** development board via **`AXI`** interface in Linux User space. 
Scope of the project is to expose Kria Zynq System Monitor(FPGA temperature, VCCINT, VCCAUX, VCCBRAM) through `AXI_LITE` and KV260 PMOD `AXI GPIO` to Linux **`uio`** User Space.

The project includes also the software part which is written in **`Python`** and **`C++(XDAQ)`** to control the PMOD GPIO and monitor the FPGA temperature. For the C++ part, the software exposes the **`REST`** endpoints to control the PMOD GPIO and monitor the System Monitor data. Also, it forwards System Monitor data (FPGA Temp, VCCINT, VCCAUX, VCCBRAM) to the **`MQTT`** message broker which is running on the same network.

## Table of Contents
[[_TOC_]]

## Prerequisites
- [Kria Zynq UltraScale+ MPSoC](https://www.xilinx.com/products/som/kria/k26c-commercial.html)
- [KV260 development board](https://www.xilinx.com/products/som/kria/kv260-vision-starter-kit.html)
- [Xilinx Vivado 2023.2](https://www.xilinx.com/products/design-tools/vivado.html)
- [PetaLinux 2023.2](https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Introduction)
- [PyHAL library](https://gitlab.cern.ch/cmsos/worksuite/-/tree/master/hal)
- [CMS DAQ XDAQ Framework](https://gitlab.cern.ch/cmsos/worksuite)

## Steps

1. **Firmware** 
    - Develop system blocks in Vivado
    - Generate BitStream (`.bit`)
    - Export Hardware (`.xsa`)
2. **PetaLinux** [Recipe](petalinux/README.md)
    -   Create a PetaLinux project using the firmware files.
    -   Build the PetaLinux project and create a bootable image.
    -   Boot the Kria Zynq UltraScale+ MPSoC with the bootable image.
3. **Software** 
    - Write a simple user space application to control the AXI GPIO and read the System Monitor values.
    - Run the software within the Kria Zynq ARM64 environment using a container which includes all the required resources to run the software.
    - Extend the software using Python and C++(XDAQ) to control the PMOD GPIO and monitor the system monitor values.

# Firmware

## Vivado Block Diagram

Create a new Vivado project and add the following IP blocks to the block diagram:
- Zynq UltraScale+ MPSoC
- Sytem Management Wizard
- AXI GPIO

![Vivado Block Diagram](./firmware/zynq.png)

## AXI Address Map

The AXI address map is as follows:
- `0xA000_0000`: AXI GPIO (1-bit)
- `0xA000_1000`: System Management Wizard (SYSMON)

![AXI Address Map](./firmware/axiAddress.png)

## Constraints

Connect the AXI GPIO to the [Kria K26 SOM](https://docs.xilinx.com/r/en-US/ds987-k26-som/SOM240_1-Signal-Names-and-Descriptions) FPGA Package PIN which corresponds to the PMOD connector(Pin PMOD_HDA11) on the KV260 development board. Setup as IOSTANDARD LVCMOS33 and output only. 
```
set_property -dict {PACKAGE_PIN H12 IOSTANDARD LVCMOS33} [get_ports {pmod1_tri_o[0]}];
```
![K26 Package & KV260 PMOD Pinout](./firmware/pinout.png)

# Petalinux

## Build the PetaLinux Project

Follow the [PetaLinux Recipe](petalinux/README.md) to create a PetaLinux project using the firmware files and build the PetaLinux project to create a bootable image.

## Linux `uio` User Space Check
Check if device tree overlay is loaded:
```
[<user>@<kria-host-name> ~]# for uio_device in /sys/class/uio/uio*/name; do     echo "$(basename $(dirname $uio_device)) Name: $(cat $uio_device)"; done
uio0 Name: axi-pmon
uio1 Name: axi-pmon
uio2 Name: axi-pmon
uio3 Name: axi-pmon
uio4 Name: gpio
uio5 Name: system_management_wiz
```

# Software

The software is a simple user space application to control the AXI GPIO and read the System Monitor values. The software is written in Python and uses the PyHAL library to access the AXI GPIO and System Monitor.

The first step is to create the ZYNQ address map with the corresponding register name, address, mask, read/write, and description. The address map is as follows:
```
...
sysmon_fpga_temperature                    00000400   ffffffffffffffff 1   0  On-device temperature measurement
...
pmod1_tri_o                                00000000   0000000000000001 1   1  GPIO PMOD1 tri-state output
...
```

The second step is to write a simple Python script using the `PyHAL` library which provides a simple interface to access the AXI interface on any device:
```python
import hal
ZYNQ_AXI_LITE_SYSMON = hal.AXIDevice("system_management_wiz@a0010000", "ZYNQAddressMap.dat")
ZYNQ_AXI_GPIO = hal.AXIDevice("gpio@a0000000", "ZYNQAddressMap.dat")

temperature_raw = ZYNQ_AXI_LITE_SYSMON.read("sysmon_fpga_temperature", 0)

print("Raw value: ",temperature_raw, "Temperature value: ", temperature_raw * (503.975 / 65536) + (-273.15))

ZYNQ_AXI_GPIO.write("pmod1_tri_o", 0x1)
sleep(5)
ZYNQ_AXI_GPIO.write("pmod1_tri_o", 0x0)
```

The software example and the address table can be found in the `software` directory.

## Run Software

Project offers two ways to run the software within the Kria Zynq ARM64 environment using a container which includes all the required resources to run the software:
- **Python**: Run using the PyHAL library using Python3.
- **XDAQ-C++**: Run using the hal library through Phase-2 CMS DAQ XDAQ Framework based on C++.

## Python
To run the software within a container environment which includes the PyHAL library, use the following command to initialize an interactive shell within the container environment. The container environment is based on the `ALMA9` image and includes the `PyHAL` library:
```
podman run --network=host --device=/dev:/dev --privileged -it --rm --pull=newer gitlab-registry.cern.ch/ptzanis/zynq-mpsoc-control/zynq-python:latest
```
Monitor the system monitor values (add `--loop` to monitor continuously):
```
[<user>@<kria-host-name>[in-container] ~]#  python3 zynqMonitor.py
Parameter    Raw        Value      Unit
Temp         0x9b58     32.67      C
VCCINT       0x3d34     0.72       V
VCCAUX       0x9916     1.79       V
VCCBRAM      0x48e8     0.85       V
----------
```
Control the PMOD GPIO(add `--loop` to loop every 1 second):
```
[<user>@<kria-host-name>[in-container] ~]#  python3 zynqControl.py
```
The full software example can be found in the [`software/zynq-python`](https://gitlab.cern.ch/ptzanis/zynq-mpsoc-control/-/tree/master/software/zynq-python?ref_type=heads) directory.

## XDAQ - C++

The software can also be run using the `XDAQ` framework and the `hal` library. The software is written in C++ and uses the `hal` library to access the AXI GPIO and System Monitor. It exposes the REST endpoints to control the PMOD GPIO and monitor the FPGA temperature. Also, it forwards System Monitor data (FPGA Temp, VCCINT, VCCAUX, VCCBRAM) to the `MQTT` message broker which is running on the same host.

To run the software within a container environment(`ALMA9` image) use the following command to initialize an interactive shell within the container environment:
```
podman run --network=host --device=/dev:/dev --privileged -it --rm --pull=newer gitlab-registry.cern.ch/ptzanis/zynq-mpsoc-control/zynq-xdaq:latest
```
When the container is initialised, the XDAQ application is initialised also and expose the following REST endpoints which can be accessed from anywhere in the network via `curl` commands:
- `curl http://<zynq-host-name>:1972/fpgaTemperature` to monitor the FPGA temperature just for debugging.
- `curl http://<zynq-host-name>:1972/gpio/0` to control the PMOD GPIO. `gpio/0` to turn OFF and `gpio/1` to turn ON.

The System Monitor data is forwarded to the `MQTT` message broker which is running on the same host. The `MQTT` available topics are which updates every 1 second are as follows:
- `sysmon/fpgaTemperature` to monitor the FPGA temperature.
- `sysmon/vccint` to monitor the VCCINT voltage.
- `sysmon/vccaux` to monitor the VCCAUX voltage.
- `sysmon/vccbram` to monitor the VCCBRAM voltage.

The full software example can be found in the [`software/zynq-xdaq`](https://gitlab.cern.ch/ptzanis/zynq-mpsoc-control/-/tree/master/software/zynq-xdaq?ref_type=heads) directory.

# Kubernetes

## "Labslice" cluster in B40

Kubernetes running on the Kria is installed using the following Ansible playbooks and roles: 
[`cms-rcms/run4-rcms-prototypes/labslice-k8s-cluster-ansible`](https://gitlab.cern.ch/cms-rcms/run4-rcms-prototypes/labslice-k8s-cluster-ansible)

It uses kuberadm to provision the cluster, with the control plane running in CERN IT OpenStack.

## Kernel configuration on the Kria

To be finalized and documented. Currently, we are using the Kernel parameters from the official Alma Linux 9 image for the Raspberry Pi 4B with hardware-specific parameters removed.

# Authors

Questions, comments, suggestions, or help?

**Polyneikis Tzanis**: polyneikis.tzanis@cern.ch

**Philipp Brummer**: philipp.maximilian.brummer@cern.ch

**Kareen Arutjunjan**: kareen.arutjunjan@cern.ch
