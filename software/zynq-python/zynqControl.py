import hal
import time
import argparse

ZYNQ_AXI_GPIO = hal.AXIDevice("gpio@a0000000", "zynqAddressMap.dat")

def controlGPIOLED(value):
    print("Setting LED to {}".format(value))
    ZYNQ_AXI_GPIO.write("pmod1_tri_o", value)
    time.sleep(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--loop", action="store_true", help="Run in loop")
    args = parser.parse_args()

    if args.loop:
        while True:
            controlGPIOLED(0x1)
            controlGPIOLED(0x0)
    else:
        controlGPIOLED(0x1)
        controlGPIOLED(0x0)