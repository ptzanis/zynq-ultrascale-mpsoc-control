import hal
import time
import argparse

ZYNQ_AXI_LITE_SYSMON = hal.AXIDevice("system_management_wiz@a0010000", "zynqAddressMap.dat")

def convert_temperature(raw_value):
    return raw_value * (503.975 / 65536) + (-273.15)

def convert_voltage(raw_value):
    return raw_value * (3.0 / 65536)

def readSYSMON():

    parameters = {
        "Temp": "sysmon_fpga_temperature",
        "VCCINT": "sysmon_fpga_vccint",
        "VCCAUX": "sysmon_fpga_vccaux",
        "VCCBRAM": "sysmon_fpga_vccbram"
    }

    raw_values = {param: ZYNQ_AXI_LITE_SYSMON.read(addr, 0) for param, addr in parameters.items()}

    print("{:<12} {:<10} {:<10} {:<10}".format("Parameter", "Raw", "Value", "Unit"))

    for param, raw_value in raw_values.items():
        if "Temp" in param:
            value = convert_temperature(raw_value)
            unit = "C"
        else:
            value = convert_voltage(raw_value)
            unit = "V"
        print("{:<12} {:<10} {:<10.2f} {:<10}".format(param, hex(raw_value), value, unit))
    print(10 * "-")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--loop", action="store_true", help="Run in loop")
    args = parser.parse_args()

    if args.loop:
        while True:
            readSYSMON()
            time.sleep(2)
    else:
        readSYSMON()
