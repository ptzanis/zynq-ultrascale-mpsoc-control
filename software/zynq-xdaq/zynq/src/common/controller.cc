/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#include "zynq/controller.h"

XDAQ_INSTANTIATOR_IMPL(zynq::controller);

zynq::controller::controller(xdaq::ApplicationStub *stub) : xdaq::Application(stub)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Hello Zynq!");
	try
	{
		HAL::AXIAddressTableASCIIReader zynqAdressTableReader("zynqAddressTable.dat");
		axiTable_ = new HAL::AXIAddressTable("DTH Addresstable", zynqAdressTableReader);
	}
	catch (HAL::HardwareAccessException &e)
	{
		std::cout << "PROGRAM WILL END!!! Could not create Addresstable for Zynq in Constructor." << std::endl;
		exit(-1);
	}

	zynqSysMon = new HAL::AXIDevice("system_management_wiz@a0010000", *axiTable_, axiBusAdapter_);
	zynqGpio = new HAL::AXIDevice("gpio@a0000000", *axiTable_, axiBusAdapter_);

	auto restateService = api::restate::getInterface(this->getApplicationContext())->join(this, this);
	restateService->registerResource("/fpgaTemperature", std::bind(&zynq::controller::fpgaTemperatureRestate, this, std::placeholders::_1));
	restateService->registerResource("/gpio/{value|[0-1]}", std::bind(&zynq::controller::toggleGpioRestate, this, std::placeholders::_1));

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void zynq::controller::actionPerformed(xdata::Event &event)
{

	try
	{
		metrisService_ = api::metris::getInterface(this->getApplicationContext())->join(this, this);
	}
	catch (api::metris::exception::Exception &e)
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), e.what());
		return;
	}

	try
	{
		metrisBag_ = metrisService_->createMetrics<xdata::DynamicBag>("sysmon", {{"fpgaTemperature", 0.0f}, {"vccint", 0.0f}, {"vccaux", 0.0f}, {"vccbram", 0.0f}}, {{"pulse-every-seconds", "2"}});
	}
	catch (api::metris::exception::Exception &e)
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), e.what());
		return;
	}

	std::thread source_thread(&zynq::controller::run, this);
	source_thread.detach();
}

void zynq::controller::run()
{

	while (1)
	{
		std::chrono::milliseconds timespan(1000);
		std::this_thread::sleep_for(timespan);

		auto data = metrisBag_->get();
		data.at<xdata::Float>("fpgaTemperature") = fpgaTemperature();
		data.at<xdata::Float>("vccint") = readVoltageItem("sysmon_fpga_vccint");
		data.at<xdata::Float>("vccaux") = readVoltageItem("sysmon_fpga_vccaux");
		data.at<xdata::Float>("vccbram") = readVoltageItem("sysmon_fpga_vccbram");
		metrisBag_->put(data);
	}
}

float zynq::controller::fpgaTemperature()
{

	uint32_t value;
	zynqSysMon->read("sysmon_fpga_temperature", &value);
	float temperature = value * 503.975 / 65536 - 273.15;

	return temperature;
}

float zynq::controller::readVoltageItem(std::string item)
{

	uint32_t value;
	zynqSysMon->read(item, &value);
	float voltage = value * 3.0 / 65536;

	return voltage;
}

const std::shared_ptr<api::restate::Response> zynq::controller::fpgaTemperatureRestate(const api::restate::Request &response)
{
	return response.createResponse(std::to_string(fpgaTemperature()), api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}

void zynq::controller::toggleGpio(uint32_t toggle)
{

	zynqGpio->write("pmod1_tri_o", toggle);
}

const std::shared_ptr<api::restate::Response> zynq::controller::toggleGpioRestate(const api::restate::Request &response)
{
	unsigned int value = std::stoi(std::string(response.getArg("value")));
	toggleGpio(value);
	std::string rensposeCall = "GPIO LED " + std::to_string(value);

	return response.createResponse(rensposeCall, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}
