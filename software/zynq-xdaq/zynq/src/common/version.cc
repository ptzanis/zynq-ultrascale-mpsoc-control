/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#include "zynq/version.h"
#include "config/version.h"
#include "xdaq/version.h"

GETPACKAGEINFO(zynqdemo)

void zynqdemo::checkPackageDependencies(){
	CHECKDEPENDENCY(config)
		CHECKDEPENDENCY(xdaq)}

std::set<std::string, std::less<std::string>> zynqdemo::getPackageDependencies()
{
	std::set<std::string, std::less<std::string>> dependencies;
	ADDDEPENDENCY(dependencies, config);
	ADDDEPENDENCY(dependencies, xdaq);
	return dependencies;
}
