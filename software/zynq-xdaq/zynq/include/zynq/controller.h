/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */
#ifndef _dev_zynq_controller_h_
#define _dev_zynq_controller_h_

#include "xdaq/Application.h"
#include "hal/AXIAddressTableASCIIReader.hh"
#include "hal/AXIAddressTable.hh"
#include "hal/AXILinuxBusAdapter.hh"
#include "hal/AXIDevice.hh"

#include "api/restate/Interface.h"
#include "api/restate/Utils.h"

#include "api/metris/Interface.h"
#include "xdata/DynamicBag.h"
#include "xdata/ActionListener.h"
#include "api/metris/Service.h"
#include "api/metris/ServiceListener.h"
#include "api/metris/Metrics.h"

namespace zynq
{

	class controller : public xdaq::Application, public api::restate::ServiceListener, public xdata::ActionListener, public api::metris::ServiceListener
	{
	public:
		XDAQ_INSTANTIATOR();
		controller(xdaq::ApplicationStub *stub);
		void actionPerformed(xdata::Event &event);
		void run();
		float fpgaTemperature();
		float readVoltageItem(std::string item);
		const std::shared_ptr<api::restate::Response> fpgaTemperatureRestate(const api::restate::Request &response);
		void toggleGpio(uint32_t toggle);
		const std::shared_ptr<api::restate::Response> toggleGpioRestate(const api::restate::Request &response);

	private:
		HAL::AXILinuxBusAdapter axiBusAdapter_;
		HAL::AXIAddressTable *axiTable_;
		HAL::AXIDevice *zynqSysMon;
		HAL::AXIDevice *zynqGpio;

	protected:
		std::shared_ptr<api::metris::Service> metrisService_;
		std::shared_ptr<api::metris::Metrics<xdata::DynamicBag>> metrisBag_;
	};
}

#endif
