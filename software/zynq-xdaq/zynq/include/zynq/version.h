/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P.Tzanis                         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#ifndef _dev_zynq__version_h_
#define _dev_zynq__version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ZYNQWORK_ZYNQDEMO_VERSION_MAJOR 1
#define ZYNQWORK_ZYNQDEMO_VERSION_MINOR 0
#define ZYNQWORK_ZYNQDEMO_VERSION_PATCH 0
// If any previous versions available E.g. #define ZYNQWORK_ZYNQDEMO_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef ZYNQWORK_ZYNQDEMO_PREVIOUS_VERSIONS

//
// Template macros
//
#define ZYNQWORK_ZYNQDEMO_VERSION_CODE PACKAGE_VERSION_CODE(ZYNQWORK_ZYNQDEMO_VERSION_MAJOR, ZYNQDEMO_ZYNQDEMO_VERSION_MINOR, ZYNQDEMO_ZYNQDEMO_VERSION_PATCH)
#ifndef ZYNQWORK_ZYNQDEMO_PREVIOUS_VERSIONS
#define ZYNQWORK_ZYNQDEMO_FULL_VERSION_LIST PACKAGE_VERSION_STRING(ZYNQWORK_ZYNQDEMO_VERSION_MAJOR, ZYNQDEMO_ZYNQDEMO_VERSION_MINOR, ZYNQDEMO_ZYNQDEMO_VERSION_PATCH)
#else
#define ZYNQWORK_ZYNQDEMO_FULL_VERSION_LIST ZYNQWORK_ZYNQDEMO_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ZYNQDEMO_ZYNQDEMO_VERSION_MAJOR, ZYNQDEMO_ZYNQDEMO_VERSION_MINOR, ZYNQDEMO_ZYNQDEMO_VERSION_PATCH)
#endif

namespace zynqdemo
{
	const std::string project = "zynqwork";
	const std::string package = "zynqdemo";
	const std::string versions = ZYNQWORK_ZYNQDEMO_FULL_VERSION_LIST;
	const std::string summary = " Kria Zynq UltraScale+ MPSoC based on XDAQ for control and monitor ZYNQ via AXI interface.";
	const std::string description = "Simple project to control Kria Zynq UltraScale+ MPSoC on KV260 development board via AXI interface in linux user space. Scope of the project is to expose Kria Zynq System Monitor(FPGA temperature, VCCINT, VCCAUX, VCCBRAM) through AXI_LITE and KV260 PMOD AXI GPIO to linux uio user space.";
	const std::string authors = "Polyneikis Tzanis";
	const std::string link = "https://gitlab.cern.ch/ptzanis/zynq-mpsoc-control";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
