--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
--Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2023.2 (win64) Build 4029153 Fri Oct 13 20:14:34 MDT 2023
--Date        : Thu Feb 22 22:12:24 2024
--Host        : POLYNEIKISTCC83 running 64-bit major release  (build 9200)
--Command     : generate_target zynq_wrapper.bd
--Design      : zynq_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity zynq_wrapper is
  port (
    pmod1_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
end zynq_wrapper;

architecture STRUCTURE of zynq_wrapper is
  component zynq is
  port (
    pmod1_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component zynq;
begin
zynq_i: component zynq
     port map (
      pmod1_tri_o(0) => pmod1_tri_o(0)
    );
end STRUCTURE;
