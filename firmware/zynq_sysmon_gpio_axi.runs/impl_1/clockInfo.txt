-------------------------------------
| Tool Version : Vivado v.2023.2
| Date         : Thu Feb 22 22:43:53 2024
| Host         : POLYNEIKISTCC83
| Design       : design_1
| Device       : xck26-sfvc784-2LV-C-
-------------------------------------

For more information on clockInfo.txt clock routing debug file see https://support.xilinx.com/s/article/000035660?language=en_US

***********************
Running Pre-CRP Checker
***********************
Number of global clocks: 1
	Number of BUFGCE: 0
	Number of BUFGCE_HDIO: 0
	Number of BUFG_CTRL: 0
	Number of BUFGCE_DIV: 0
	Number of BUFG_GT: 0
	Number of BUFG_PS: 1
	Number of BUFG_FABRIC: 0
Pre-CRP Checker took 0 secs

********************************
Clock Net Route Info (CRP Input)
********************************
Clock 1: zynq_i/zynq_ultra_ps_e_0/U0/pl_clk0
	Clock source type: BUFG_PS
	Clock source region: X0Y3
	Clock regions with locked loads: (0, 1) (0, 2) (2, 3) 
	initial rect ((0, 1), (2, 3))



*****************
User Constraints:
*****************
No user constraints found


