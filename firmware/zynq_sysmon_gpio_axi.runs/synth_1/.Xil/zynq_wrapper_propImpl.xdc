set_property SRC_FILE_INFO {cfile:z:/Documents/CMS/Work_Developments/Firmware/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi.gen/sources_1/bd/zynq/ip/zynq_system_management_wiz_0_0/zynq_system_management_wiz_0_0.xdc rfile:../../../zynq_sysmon_gpio_axi.gen/sources_1/bd/zynq/ip/zynq_system_management_wiz_0_0/zynq_system_management_wiz_0_0.xdc id:1 order:EARLY scoped_inst:zynq_i/system_management_wiz_0/U0} [current_design]
set_property SRC_FILE_INFO {cfile:z:/Documents/CMS/Work_Developments/Firmware/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi.gen/sources_1/bd/zynq/ip/zynq_rst_ps8_0_99M_0/zynq_rst_ps8_0_99M_0.xdc rfile:../../../zynq_sysmon_gpio_axi.gen/sources_1/bd/zynq/ip/zynq_rst_ps8_0_99M_0/zynq_rst_ps8_0_99M_0.xdc id:2 order:EARLY scoped_inst:zynq_i/rst_ps8_0_99M/U0} [current_design]
set_property SRC_FILE_INFO {cfile:Z:/Documents/CMS/Work_Developments/Firmware/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi/zynq_sysmon_gpio_axi.srcs/constrs_1/new/pmod_pinout.xdc rfile:../../../zynq_sysmon_gpio_axi.srcs/constrs_1/new/pmod_pinout.xdc id:3} [current_design]
current_instance zynq_i/system_management_wiz_0/U0
set_property src_info {type:SCOPED_XDC file:1 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC SYSMONE4_X0Y0 [get_cells -hier {*inst_sysmon} -filter {NAME =~ *inst_sysmon}]
current_instance
current_instance zynq_i/rst_ps8_0_99M/U0
set_property src_info {type:SCOPED_XDC file:2 line:50 export:INPUT save:INPUT read:READ} [current_design]
create_waiver -type CDC -id {CDC-11} -user "proc_sys_reset" -desc "Timing uncritical paths" -tags "1171415" -scope -internal -to [get_pins -quiet -filter REF_PIN_NAME=~*D -of_objects [get_cells -hierarchical -filter {NAME =~ */ACTIVE_LOW_AUX.ACT_LO_AUX/GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to}]]
current_instance
set_property src_info {type:XDC file:3 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN H12 IOSTANDARD LVCMOS33} [get_ports {pmod1_tri_o[0]}];
