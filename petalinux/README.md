# Recipe for Petalinux
Credits to: Kareen, Philipp and Petr

- Get KV260 board BSP file from [Xilinx](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html)
- `petalinux-create -t project -s <path-to-bsp>`
- `petalinux-config --get-hw-description=<path-to-xsa>`
- `petalinux-build -c device-tree` to generate the `pl.dtsi` used in the next step
- Copy the entire `axi_gpio_0` and `system_management_wiz_0` blocks from within the `&amba {...}` block of `components/plnx_workspace/device-tree/device-tree/pl.dtsi` to an `&amba {...}` block inside `components/plnx_workspace/device-tree/device-tree/system-user.dtsi`
    - Replace the existing `compatible` with `compatible = "generic-uio";` for the copied `axi_gpio_0` and `system_management_wiz_0`
- `petalinux-build`
- `cd images/linux/`
- `dtc -I dtb -O dts -o system.dts system.dtb` if you want to examine the generated device tree
- `echo -n "<revision-info>" | xxd -ps > revision_info.txt` to set revision info
- `petalinux-package --force --boot --u-boot --fpga <path-to-bit> --bif-attribute udf_bh --bif-attribute-value revision_info.txt`
- Copy the contents of `images/linux/` to the tftboot folder of the board
- Unpack `images/linux/rootfs.tar.gz` and copy the contents to the NFS root of the board (or synchronize changed parts)
- Copy the `images/linux/BOOT.BIN` to the NFS of the board (e.g. to `/root/BOOT.bin`)
- Log in to Kria board
- `image_update -i /root/BOOT.BIN`
- `image_update -p`
- `reboot by pressing reset button`
- `image_update -p`
- `image_update -v`

